Final task ISS 2015

STEP 1

Design and build a (prototype of a) software system that, with reference to a differential drive robot (called from now on robot)

	- allows an user to select between a 'learnig phase' and a 'autonomous phase'
	- during the learnig phase, the user can send a sequence of move commands (e.g. forwad, backward, left right, stop) to the robot. The robot must not only execute each command but it must also record the whole sequence of commands until the user decides to terminate the learning phase.
	- after the termination of the learning phase, the user can tell the robot to enter the autonomous phase in a 'direct' or in a 'reverse' mode. During this phase the robot executes in autonomous way the sequence of moves it has learned, by complementing each move (e.g. forward->backward) if the selected mode is reverse
	- during the autonomous phase, the robot must be able to excute (as soon as possible) a stop command send by the user

Non functional requriments at step1

	- remember to express in explicit way the technological hypothesis assumed during the problem analysis phase, to define the abstraction gap (if any) and to explain how the software project can overcome (in a repetable way) such a gap
	

STEP 2 (Optional)

After the development of this prototype, consider the possibility to enhance the functional capabilities of the robot, by allowing it :

	- to perceive an obstacle during the autonomous phase amd, once the obstacle is detected to execute some alternative behavior (in term of moves)

Non functional requirements at step2
	
	- the main goal is to discuss how some change (monotonic extensions) of the requirements impact on a product whose production is based on 'formal' , 'technology independent' artifacts rather than on ad-hoc code.

During this phase, the software could optionally define some modification/extension to the QA/DDR DSL in order to fulfill specific goal of interest of the team.

By AN Unibo-DISI