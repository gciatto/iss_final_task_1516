//QActorWebUI.js
var curSpeed = "Low";
	
    console.log("QActorWebUI.js : server IP= "+document.location.host);
 /*
 * WEBSOCKET
 */
    var sock = new WebSocket("ws://"+document.location.host, "protocolOne");
    sock.onopen = function (event) {
         //console.log("QActorWebUI.js : I am connected to server.....");
         document.getElementById("connection").value = 'CONNECTED';
    };
    sock.onmessage = function (event) {
        //console.log("QActorWebUI.js : "+event.data);
        //alert( "onmessage " + event);
        document.getElementById("state").value = ""+event.data;
    }
    sock.onerror = function (error) {
        //console.log('WebSocket Error %0',  error);
        document.getElementById("state").value = ""+error;
    };
    
	function setSpeed(val){
		curSpeed = val;
		document.getElementById("speed").value = curSpeed;
	}
	function send(message) {
		document.getElementById("sending").value = ""+message;
		sock.send(message);
	};
	
	document.onkeydown = function(event) {
//	alert("event="+event);
		    event = event || window.event;
		    switch (event.keyCode || event.which) {
		        case 65:
		            send('left');
		            break;
		        case 68:
		            send('right');
		            break;
		        case 87:
		            send('forward');
		            break;
		        case 83:
		            send('backward');
		            break;
		        default:
		            send('stop');
		    }
	};
 	//alert("loaded");
