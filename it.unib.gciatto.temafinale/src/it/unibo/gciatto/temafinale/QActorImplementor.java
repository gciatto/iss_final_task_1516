package it.unibo.gciatto.temafinale;


import java.lang.ref.WeakReference;

import alice.tuprolog.Prolog;
import it.unibo.qactors.QActor;

/**
 * A QActorImplementor is an object associated to a QActor.
 * Its aim is to implement the actor's custom actions in order to 
 * prevent their deletion
 * 
 * @author gciatto
 */
public class QActorImplementor {
	private final WeakReference<QActor> actor;
	private final Prolog testEngine; 
	
	public QActorImplementor(QActor actor) {
		this.actor = new WeakReference<>(actor);
		testEngine = actor == null ? new Prolog() : null; 
	}
	
	
	
	public QActor getActor() {
		return actor.get();
	}
	
	public Prolog getEngine() {
		final QActor qa = actor.get();
		if (qa != null) {
			return qa.getPrologEngine();
		} else {
			return testEngine;
		}
	}
}
