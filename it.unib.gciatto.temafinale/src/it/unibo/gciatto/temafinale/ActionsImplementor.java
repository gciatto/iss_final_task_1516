package it.unibo.gciatto.temafinale;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.SolveInfo;
import it.unibo.qactors.QActor;

public class ActionsImplementor extends QActorImplementor {
	private static final String EXEC_TEMPLATE = "executing('%s', %s, '%s')";

	public enum Phase {
		LEARN, DIRECT, INVERSE /* , IDLE */;
	}

	private final List<String> actions = new LinkedList<>();
	private final List<Long> durations = new LinkedList<>();
	private long lastInstant;
	private int actionPointer = 0;
	private Phase phase = Phase.LEARN;

	public ActionsImplementor(QActor actor) {
		super(actor);

		getEngine().addExceptionListener(ee -> {
			System.err.println(ee.getMsg());
			getActor().println(ee.getMsg());
		});

		getEngine().addOutputListener(oe -> {
			System.out.println(oe.getMsg());
			getActor().println(oe.getMsg());
		});
	}
	
	@Deprecated
	public ActionsImplementor() {
		this(null);
	}

	public void learnUserInput(String command) {
		if (phase == Phase.LEARN) {
			long currInstant = System.currentTimeMillis();
			actions.add(command);
			if (actions.size() > 1) {
				durations.add(currInstant - lastInstant);
			}
			lastInstant = currInstant;
		} else {
			throw new IllegalStateException();
		}
	}

	public List<String> getActionSequence() {
		return Collections.unmodifiableList(actions);
	}
	
	public List<Long> getDurationSequence() {
		return Collections.unmodifiableList(durations);
	}

	public void startLearning() {
		actions.clear();
		durations.clear();
		phase = Phase.LEARN;
	}

	public void startAutonomousDirect() {
		if (phase == Phase.LEARN) {
			phase = Phase.DIRECT;
			actionPointer = 0;
			durations.add(System.currentTimeMillis() - lastInstant);
		} else {
			throw new IllegalStateException();
		}
	}

	public void startAutonomousInverse() {
		if (phase == Phase.LEARN) {
			phase = Phase.INVERSE;
			actionPointer = actions.size() - 1;
			durations.add(System.currentTimeMillis() - lastInstant);
		} else {
			throw new IllegalStateException();
		}
	}

	public boolean getNextMove() {
		if (phase != Phase.LEARN) {
			@SuppressWarnings("unused")
			SolveInfo si;
			try {
				if (hasAction()) {
					si = getEngine().solve("asserta(" + getAction() + ").");
				} else {
					return false;
				}
				nextAction();
				return true;
			} catch (MalformedGoalException e) {
				throw new IllegalStateException(e);
			} 
		} else {
			throw new IllegalStateException();
		}
	}
	
	public Phase getPhase() {
		return phase;
	}

	protected String getAction() {
		return String.format(EXEC_TEMPLATE, actions.get(actionPointer), durations.get(actionPointer),
				phase.name().toLowerCase());
	}

	protected boolean hasAction() {
		switch (phase) {
		case LEARN:
			throw new IllegalStateException();
		case DIRECT:
			return actionPointer < actions.size();
		case INVERSE:
			return actionPointer >= 0;
		default:
			return false;
		}
	}

	protected void nextAction() {
		switch (phase) {
		case LEARN:
			throw new IllegalStateException();
		case DIRECT:
			actionPointer++;
			break;
		case INVERSE:
			actionPointer--;
			break;
		}
	}
}
