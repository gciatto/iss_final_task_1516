package it.unibo.gciatto.temafinale;

import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.qactors.web.QActorHttpServer;

public class QActorDashboardContextHttpServer extends QActorHttpServer {

	public QActorDashboardContextHttpServer(IOutputEnvView outEnvView, String dirPath, int port) {
		super(outEnvView, dirPath, port);
	}
	
	public static final String stopEvId = "stop";
	public static final String stopPayloadPattern = "usercmd(\"%s\")";

	@Override
	protected void handleUserCmd(String cmd) throws Exception {
		if (cmd != null && cmd.contains("stop")) {
			String eventMsg = String.format(stopPayloadPattern, cmd);
			outEnvView.addOutput("--- QActorHttpServer cmd: " + cmd + " emits " + stopEvId + ":" + eventMsg);
			platform.raiseEvent("wsock", stopEvId, eventMsg);
		} else {
			super.handleUserCmd(cmd);
		}
	}

}
