package it.unibo.gciatto.temafinale;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import it.unibo.iot.models.sensorData.distance.IDistanceSensorData;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.robot_actor.SensorObserver;

public class DistanceSensorObserver extends SensorObserver<IDistanceSensorData> {
	
	private static class DistanceSensorGui extends JFrame {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1825886625901222602L;
		
		public static final String DATA_LABEL_FORMAT = "Distance sensor: %s";
		public static final String THRESHOLD_LABEL_FORMAT = "Threshold: %s";
		
		private final JLabel labelSensor, labelThreshold;
		private final JSlider slider;
		
		public DistanceSensorGui() {
			setAlwaysOnTop(true);
			
			final JPanel contentPane = new JPanel();
			setContentPane(contentPane);
			
			contentPane.setLayout(new BorderLayout());
			
			labelThreshold = new JLabel(String.format(THRESHOLD_LABEL_FORMAT, C.sensors.distance.getThreshold()));
			contentPane.add(labelThreshold, BorderLayout.NORTH);
			
			labelSensor = new JLabel(String.format(DATA_LABEL_FORMAT, 000));
			contentPane.add(labelSensor, BorderLayout.CENTER);
			
			slider = new JSlider(JSlider.HORIZONTAL, -1, C.sensors.distance.FAR * 2, C.sensors.distance.getThreshold());
			slider.setMajorTickSpacing(10);
			slider.setMinorTickSpacing(1);
			slider.setPaintTicks(true);
			slider.setPaintLabels(true);
			contentPane.add(slider, BorderLayout.SOUTH);
			
			slider.addChangeListener(e -> {
				final int t = slider.getValue();
				C.sensors.distance.setThreshold(t);
				labelThreshold.setText(String.format(THRESHOLD_LABEL_FORMAT, C.sensors.distance.getThreshold()));
			});
			
			SwingUtilities.invokeLater(() -> {
				pack();
				setVisible(true);
			});
		}
		
		public void notify(int distance) {
			SwingUtilities.invokeLater(() -> labelSensor.setText(String.format(DATA_LABEL_FORMAT, distance))); 
		}
	}
	
	private DistanceSensorGui gui;

	public DistanceSensorObserver(IOutputView outView) {
		super(outView);
		
		if (C.sensors.distance.SIMULATED) {
			gui = new DistanceSensorGui();
		}
	}

	/**
	 * Generates an event if and only if an obstacle is closer than C.sensors.distance.THRESHOLD
	 */
	@Override public void notify(IDistanceSensorData data) {
		final int distance = data.getDistance().getDistanceValue().getValue();
		if (distance <= C.sensors.distance.getThreshold()) {
			if (gui != null) {
				gui.notify(distance);
			}
			platform.raiseEvent("sensor", "obstacle", data.getDefStringRep() );
		}
	}
	
	
}
