package it.unibo.gciatto.temafinale;

import it.unibo.iot.models.sensorData.distance.DistanceValue;

public class C {
	
	/**
	 * Sensor constants
	 * @author mfrancia
	 *
	 */
	public static class sensors {

		/**
		 * Distance sensor constants
		 * @author mfrancia
		 *
		 */
		public static class distance {
			
			/**
			 * Distance sensor threshold, an obstacle below such value is perceived as close
			 */
//			public static final int THRESHOLD = DistanceValue.CLOSE.getValue();
			
			// Used to disable 'obstacle' event without manipulating anything else
			private static int threshold = DistanceValue.MEDIUM.getValue(); // -1;
			
			public static boolean SIMULATED = false;
			
			public static synchronized int getThreshold() {
				return threshold;
			}
			public static synchronized void setThreshold(int threshold) {
				distance.threshold = threshold;
			}
			
			public static final int CLOSE = DistanceValue.CLOSE.getValue(); // -1;
			public static final int MEDIUM = DistanceValue.MEDIUM.getValue(); // -1;
			public static final int FAR = DistanceValue.FAR.getValue(); // -1;
			
		}
	}
}
