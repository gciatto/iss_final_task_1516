package it.unibo.gciatto.temafinale;

import java.util.Arrays;
import java.util.List;

import it.unibo.dashboard_context.MainDashboard_context;
import it.unibo.robot_context.MainRobot_context;

public class MainTemafinale {
	
	@FunctionalInterface
	interface Launchable {
		void main(String[] args) throws Exception;
	}

	public static final String help = "Syntax:\n"
			+ "\tjava -jar <jar_name>.jar [-t <int>] [-s <boolean>] [-d | -r | -b]\n" + "where\n"
			+ "\t-t sets the distance threshold, default is " + C.sensors.distance.getThreshold() + "\n"
			+ "\t-s shows the distance sensor emulator gui, default is " + C.sensors.distance.SIMULATED + "\n"
			+ "\t-d executes the dashboard_context\n" + "\t-s executes the robot_context\n"
			+ "\t-b executes both contexts, i.e. the system.\n" + "The absence of flags shows this message.";

	public static void main(String[] args) {
		Integer th = null;
		Boolean sim = null;
		boolean dash = false;
		boolean robot = false;
		boolean sys = false;

		int index = -1;

		final List<String> argsList = Arrays.asList(args);

		try {
			index = argsList.indexOf("-t");
			if (index >= 0) {
				th = new Integer(Integer.parseInt(args[index + 1]));
				C.sensors.distance.setThreshold(th);
			}

			index = argsList.indexOf("-s");
			if (index >= 0) {
				sim = new Boolean(Boolean.parseBoolean(args[index + 1]));
				C.sensors.distance.SIMULATED = sim;
			}

			if ((index = argsList.indexOf("-d")) >= 0) {
				MainDashboard_context.main(args);
			} else if ((index = argsList.indexOf("-r")) >= 0) {
				MainRobot_context.main(args);
			} else if ((index = argsList.indexOf("-b")) >= 0) {
				Thread td = new Thread(toRunnable(MainDashboard_context::main));
				Thread tr = new Thread(toRunnable(MainRobot_context::main));
				
				tr.start();
				Thread.sleep(1000);
				td.start();
				
				tr.join();
				td.join();
			} else {
				showHelp();
				System.exit(2);
			}
		} catch (Throwable ex) {
			showHelp();
			System.exit(1);
		}

	}

	static void showHelp() {
		System.out.println(help);
	}

	static Runnable toRunnable(Launchable l, String... args) {
		return () -> {
			try {
				l.main(args);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		};
	}

}
