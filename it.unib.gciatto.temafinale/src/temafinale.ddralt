RobotSystem temafinale -regeneratesrc

/*** user commands */ 
Event usercmd : usercmd(CMD)
Event stop : usercmd(CMD) // stop specializes usercmd
/*** sensor events */
Event sensordata : sensordata(_) // generic event arrangement
Event obstacle : distance(DISTANCE, DIRECTION, POSITION) // obstacle specializes sensordata

Context dashboard_context ip[host='localhost' port=20504] -g green -httpserver

EventHandler on_stop for stop { memo currentEvent for robot_actor };
EventHandler on_usercmd for usercmd { memo currentEvent for robot_actor };
EventHandler on_obstacle for obstacle { memo currentEvent for robot_actor };
//EventHandler on_sensordata for sensordata { memo currentEvent for robot_actor };


Robot mock QActor robot_actor context dashboard_context {
	
	Rules {
		//Removes every information that matches the input pattern
		cleanupRules(Pattern) :- retractall(Pattern).
		
		evalTheGuard(existAndRemove(G)) :- evalGuard(G), !, retract(G).
		evalTheGuard(exist(G)) :- evalGuard(G), !.
		evalTheGuard(G) :- evalGuard(G), !.
		
		conj(A, B) :-
			evalTheGuard(A),
			evalTheGuard(B)
		.
	}
	
	Action action_learn_user_input maxtime(0) arg user_input(_);
	Action action_start_learning maxtime(0);
	Action action_start_autonomous_direct maxtime(0);
	Action action_start_autonomous_inverse maxtime(0);
	Action action_get_next_move maxtime(0);
	
	Plan init normal
		println("init");
		// robot behavior begins with the learning phase
		switchToPlan start_learning_phase
//		switchToPlan waiting_user_input
	
	Plan waiting_user_input
		println("waiting_user_input");
		
		// waiting for input events for 5 mins
		sense time(300000) usercmd, stop -> continue, continue;
		
		[??stored(usercmd, usercmd('learning'))] switchToPlan learning_user_input;
		[??stored(usercmd, usercmd('autonomous_direct'))] switchToPlan waiting_user_input;
		[??stored(usercmd, usercmd('autonomous_inverse'))] switchToPlan waiting_user_input;
		[!?stored(_, usercmd(_))] switchToPlan learning_user_input;
		println("*** PROBABLE RACE CONDITION ***");
		switchToPlan waiting_user_input
		
	Plan learning_user_input
		println("learning_user_input");
		
		// waiting for input events for 5 mins
		[not !?stored(_, usercmd(_))] sense time(300000) usercmd, stop -> continue, continue;
		
		[??stored(usercmd, usercmd('autonomous_direct'))] switchToPlan start_autonomous_direct_phase;
		[??stored(usercmd, usercmd('autonomous_inverse'))] switchToPlan start_autonomous_inverse_phase;
		[!?stored(usercmd, usercmd('learning'))] switchToPlan start_learning_phase;
		// TODO implement closures manually
		[!?stored(_, usercmd(CMD))] execute action_learn_user_input with user_input(CMD);
		
		switchToPlan handling_motion_1;
		repeatPlan 0 // switchToPlan learning_user_input
		
		
	Plan handling_motion_1 resumeLastPlan
		println("handling_motion_1");
		
		// robot* speed (_) time (0) return immediately
		// robot* speed (_) time (T) return after T ms
		// speed(VALUE) VALUE is a percentage!
		
		// action: user input
		[??stored(usercmd, usercmd('forward'))] robotForward speed(100) time (0);
		[??stored(usercmd, usercmd('backward'))] robotBackward speed(100) time (0);
		[??stored(usercmd, usercmd('right'))] robotRight speed(100) time (0);
		[??stored(usercmd, usercmd('left'))] robotLeft speed(100) time (0);
		[??stored(_, usercmd('stop'))] robotStop speed(100) time (0) //;
		
	Plan handling_motion_2 resumeLastPlan
		println("handling_motion_2");
		
		[!?stored(obstacle, distance(_, _, _))] switchToPlan obstacle_detected;
		
		// action: direct plan
		[!? conj(
				existAndRemove(executing('forward', T, 'direct')),
				exist(stored(obstacle, distance(_, _, _)))
		)] robotForward speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		[??executing('backward', T, 'direct')] robotBackward speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		[??executing('right', T, 'direct')] robotRight speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		[??executing('left', T, 'direct')] robotLeft speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
			
		// action: inverse plan
		[??executing('forward', T, 'inverse')] robotBackward speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		[??executing('backward', T, 'inverse')] robotForward speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		[??executing('right', T, 'inverse')] robotLeft speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		[??executing('left', T, 'inverse')] robotRight speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected; // or
//					event sensordata 	-> sensordata_detected;
		
		[??executing('stop', T, _)] robotStop speed(100) time (T)
			react 	event stop 			-> start_learning_phase or
					event obstacle 		-> obstacle_detected // or // is it useful if the robot is not moving? yes! behavior uniformity
//					event sensordata 	-> sensordata_detected
		// return to the previous state
		
	Plan start_autonomous_direct_phase
		println("start_autonomous_direct_phase");
		switchToPlan cleanup_rules;
		execute action_start_autonomous_direct;
		switchToPlan autonomous_phase
	
	Plan start_autonomous_inverse_phase
		println("start_autonomous_inverse_phase");
		switchToPlan cleanup_rules;
		execute action_start_autonomous_inverse;
		switchToPlan autonomous_phase
		
	Plan cleanup_rules resumeLastPlan
		println("removing stored data");
		solve cleanupRules(stored(_, _)) time(0)
	
	Plan autonomous_phase
		println("autonomous_phase");
		execute action_get_next_move;
		[not !?executing(_, _, _)] switchToPlan start_learning_phase;
		switchToPlan handling_motion_2;
		repeatPlan 0
		
	Plan start_learning_phase
		println("start_learning_phase");
		switchToPlan cleanup_rules;
		execute action_start_learning;
//		[??stored(usercmd, usercmd('learning'))] switchToPlan waiting_user_input; // unuseful because of the plan 'cleanup_rules'
		switchToPlan waiting_user_input
		
	Plan obstacle_detected
		println("obstacle_detected");
		[??stored(obstacle, distance(_, _, _))] robotStop speed(100) time(0);
		sound time(10000) file("res/audio/on_obstacle.wav");
		switchToPlan start_learning_phase 
	
	Plan sensordata_detected resumeLastPlan
		println("sensordata_detected")
}