/* Generated by AN DISI Unibo */ 
package it.unibo.dashboard_context;
import it.unibo.qactors.ActorContext;
import java.io.InputStream;

import it.unibo.gciatto.temafinale.QActorDashboardContextHttpServer;
import it.unibo.is.interfaces.IOutputEnvView;
import it.unibo.system.SituatedSysKb;
public class MainDashboard_context extends ActorContext{
 
	public MainDashboard_context(String name, IOutputEnvView outEnvView,
			InputStream sysKbStream, InputStream sysRulesStream) throws Exception {
		super(name, outEnvView, sysKbStream, sysRulesStream);
		this.outEnvView = outEnvView;
 	}
	@Override
	public void configure() {
		try {
		println("Starting the http server ... ");
		new  QActorDashboardContextHttpServer(outEnvView,"./srcMore/it/unibo/dashboard_context",8080).start();
		println("Starting the handlers .... ");
		println("Starting the actors .... ");
		
 		} catch (Exception e) {
 		  e.printStackTrace();
		} 		
	}
 
	
/*
* ----------------------------------------------
* MAIN
* ----------------------------------------------
*/
	public static void main(String[] args) throws Exception{
		IOutputEnvView outEnvView = SituatedSysKb.standardOutEnvView;
		it.unibo.is.interfaces.IBasicEnvAwt env = new it.unibo.baseEnv.basicFrame.EnvFrame( "Env_dashboard_context", java.awt.Color.green , java.awt.Color.black );
		env.init();
		outEnvView = env.getOutputEnvView();
 		InputStream sysKbStream    = //MainDashboard_context.class.getResourceAsStream("temafinale.pl");
 			new java.io.FileInputStream("./srcMore/it/unibo/dashboard_context/temafinale.pl");
		InputStream sysRulesStream = MainDashboard_context.class.getResourceAsStream("sysRules.pl");
		new MainDashboard_context("dashboard_context", outEnvView, sysKbStream, sysRulesStream ).configure();
 	}
}
