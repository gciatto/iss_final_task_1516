package it.unibo.gciatto.temafinale.test;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Random;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.SolveInfo;
import it.unibo.gciatto.temafinale.ActionsImplementor;
import it.unibo.gciatto.temafinale.ActionsImplementor.Phase;

public class TestActionsImplementor {
	
	private static final String STOP = "stop";

	private static final String LEFT = "left";

	private static final String RIGHT = "right";

	private static final String BACKWARD = "backward";

	private static final String FORWARD = "forward";
	
	private static final String[] DIRECTIONS = new String[] {
			FORWARD,
			LEFT,
			RIGHT,
			BACKWARD,
			STOP
	};
	
	public static final String EXEC_TEMPLATE = "executing(%s, %s, %s)";
	
	public static String executingQuery(String dir, Long dur, Phase phase) {
		return String.format(EXEC_TEMPLATE + ".", 
				dir == null ? "Direction" : dir, 
				dur == null ? "Time" : dur,
				phase == null ? "Phase" : phase.name().toLowerCase()
			);
	}
	
	private static final Random rand = new Random();
	
	private static List<String> generateRandomMoveSeq(int length) {
		return rand.ints(length)
				.map(Math::abs)
				.map(it -> it % DIRECTIONS.length)
				.mapToObj(it -> DIRECTIONS[it])
				.collect(Collectors.toList());
	}
	
	private static List<Long> generateRandomDurSeq(int length, int min, int max) {
		return rand.longs(length)
				.map(Math::abs)
				.map(it -> it % (max - min) + min)
				.boxed()
				.collect(Collectors.toList());
	}
	
	private ActionsImplementor ai;
	private List<String> moves;
	private List<Long> durs;
	
	private static String inverseMove(List<String> moves, int i) {
		return moves.get(moves.size() - 1 - i);
	}
	
	private static Long inverseDur(List<Long> durs, int i) {
		return durs.get(durs.size() - 1 - i);
	}
	
	private static String directMove(List<String> moves, int i) {
		return moves.get(i);
	}
	
	private static Long directDur(List<Long> durs, int i) {
		return durs.get(i);
	}
	
	private static int CMD_SEQ_LENGTH = 10;
	private static int MIN_CMD_DUR = 500;
	private static int MAX_CMD_DUR = 2000;
	

	@SuppressWarnings("deprecation")
	@Before
	public void setUp() throws Exception {
		ai = new ActionsImplementor();
		moves = generateRandomMoveSeq(CMD_SEQ_LENGTH);
		durs = generateRandomDurSeq(CMD_SEQ_LENGTH, MIN_CMD_DUR, MAX_CMD_DUR);
	}

	@Test
	public void testLearnUserInput() {
		String move = directMove(moves, 0);
		ai.learnUserInput(move);
		assertEquals(move, ai.getActionSequence().get(0));
		long dur = directDur(durs, 0);
		sleep(dur);
		for (int i = 1; i < CMD_SEQ_LENGTH; i++) {
			move = directMove(moves, i);
			ai.learnUserInput(move);
			assertEquals(move, ai.getActionSequence().get(i));
			assertInTime(dur, ai.getDurationSequence().get(i - 1));
			dur = directDur(durs, i);
			sleep(dur);
		}
	}
	
	private static long ACCEPTABLE_DELAY = 1000l;
	
	public void assertInTime(long expected, long actual) {
		assertTrue(actual - expected < ACCEPTABLE_DELAY);
	}
	
	private static void sleep(long dur) {
		try {
			Thread.sleep(dur);
		} catch (InterruptedException e) {
			throw new IllegalStateException(e);
		}
	}

	@Test
	public void testStartLearning() {
		ai.startLearning();
		assertEquals(Phase.LEARN, ai.getPhase());
		assertEquals(0, ai.getActionSequence().size());
		assertEquals(0, ai.getDurationSequence().size());
	}

	@Test
	public void testStartAutonomousDirect() {
		assertEquals(Phase.LEARN, ai.getPhase());
		ai.startAutonomousDirect();
		assertEquals(Phase.DIRECT, ai.getPhase());
	}

	@Test
	public void testStartAutonomousInverse() {
		assertEquals(Phase.LEARN, ai.getPhase());
		ai.startAutonomousDirect();
		assertEquals(Phase.DIRECT, ai.getPhase());
	}

	@Test
	public void testGetNextMoveDirect() {
		List<String> moves = ai.getActionSequence();
		List<Long> durs = ai.getDurationSequence();
		System.out.println("*** Starting testGetNextMoveDirect ***");
		System.out.println("\tsequence to learn: " + this.moves);
		System.out.println("\tdurations to learn: " + this.durs);
		try {
			testGetNextMoveStub(ai::startAutonomousDirect, i -> directMove(moves, i), i -> directDur(durs, i), Phase.DIRECT);
		} catch (MalformedGoalException e) {
			fail(e.getMessage());
		}
		System.out.println("*** Ended testGetNextMoveDirect ***\n");
	}
	
	private void testGetNextMoveStub(Runnable switchTo, IntFunction<String> getMove, IntFunction<Long> getDur, Phase ph) throws MalformedGoalException {
		SolveInfo si;
		String move;
		long dur;
		learnEveryThingAndThenSwitch(ph);
		for (int i = 0; i < CMD_SEQ_LENGTH; i++) {
			move = getMove.apply(i);
			dur = getDur.apply(i);
			System.out.printf("\ttesting move %s: %s-%s\n", i, move, dur);
			ai.getNextMove();
			si = ai.getEngine().solve(executingQuery(move, dur, ph));
			assertTrue(si.isSuccess());
		}
	}
	
	private void learnEveryThingAndThenSwitch(Phase ph) {
		for (int i = 0; i < CMD_SEQ_LENGTH; i++) {
			ai.learnUserInput(directMove(moves, i));
			sleep(directDur(durs, i));
		}
		if (ph == Phase.DIRECT) {
			ai.startAutonomousDirect();
			System.out.println("\tstarted DIRECT");
		} else if (ph == Phase.INVERSE) {
			ai.startAutonomousInverse();
			System.out.println("\tstarted INVERSE");
		} else {
			throw new IllegalArgumentException();
		}
		System.out.println("\tdurations learned: " + ai.getDurationSequence());
	}
	
	@Test
	public void testGetNextMoveWhileLearning() {
		assertEquals(Phase.LEARN, ai.getPhase());
		try {
			ai.getNextMove();
			fail();
		} catch (IllegalStateException ex) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetNextMoveInverse() {
		List<String> moves = ai.getActionSequence();
		List<Long> durs = ai.getDurationSequence();
		System.out.println("*** Starting testGetNextMoveInverse ***");
		System.out.println("\tsequence to learn: " + this.moves);
		System.out.println("\tdurations to learn: " + this.durs);
		try {
			testGetNextMoveStub(ai::startAutonomousDirect, i -> inverseMove(moves, i), i -> inverseDur(durs, i), Phase.INVERSE);
		} catch (MalformedGoalException e) {
			fail(e.getMessage());
		}
		System.out.println("*** Ended testGetNextMoveInverse ***\n");
	}

}
